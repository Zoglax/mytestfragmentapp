package com.example.mytestfragmentapp;

import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.mytestfragmentapp.commonentities.DataTransferClass;

public class BioFragment extends Fragment {

    Button buttonApplyBio;
    EditText editTextName;
    Spinner spinnerRace;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bio, container, false);

        buttonApplyBio = view.findViewById(R.id.buttonApplySkills);
        editTextName = view.findViewById(R.id.editTextName);

        spinnerRace = view.findViewById(R.id.spinnerRace);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(),android.R.layout.simple_spinner_dropdown_item, new String[]{"Man", "Elf", "Dwarf"});
        spinnerRace.setAdapter(adapter);

        buttonApplyBio.setOnClickListener(buttonApplyBioOnClick);

        return view;
    }

    View.OnClickListener buttonApplyBioOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.Character.Name = editTextName.getText().toString();
            DataTransferClass.Character.Race = spinnerRace.getSelectedItem().toString();

            DataTransferClass.TextViewCharacterInfo.setText(DataTransferClass.Character.getStringInfo());
        }
    };
}

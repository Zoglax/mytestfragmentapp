package com.example.mytestfragmentapp.commonentities;

public class Character
{
    public String Name;
    public String Race;
    public int Power;
    public int Agility;
    public int Mana;

    public Character()
    {
        Name = "";
        Race = "";
        Power = Agility = Mana = 0;
    }

    public String getStringInfo()
    {
        return "Name: "+Name+"\n"+
               "Race: "+Race+"\n"+
               "Power: "+Power+"\n"+
               "Agility: "+Agility+"\n"+
               "Mana: "+Mana;
    }
}

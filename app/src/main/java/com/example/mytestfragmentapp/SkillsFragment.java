package com.example.mytestfragmentapp;

import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.mytestfragmentapp.commonentities.DataTransferClass;

public class SkillsFragment extends Fragment {

    Button buttonApplySkills;
    TextView textViewPower, textViewAgility, textViewMana;
    SeekBar seekBarPower, seekBarAgility, seekBarMana;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_skills, container, false);

        buttonApplySkills = view.findViewById(R.id.buttonApplySkills);

        textViewPower = view.findViewById(R.id.textViewPower);
        textViewAgility = view.findViewById(R.id.textViewAgility);
        textViewMana = view.findViewById(R.id.textViewMana);

        seekBarPower = view.findViewById(R.id.seekBarPower);
        seekBarAgility = view.findViewById(R.id.seekBarAgility);
        seekBarMana = view.findViewById(R.id.seekBarMana);

        buttonApplySkills.setOnClickListener(buttonApplySkillsOnClick);

        seekBarPower.setOnSeekBarChangeListener(seekBarPowerOnSeek);
        seekBarAgility.setOnSeekBarChangeListener(seekBarAgilityOnSeek);
        seekBarMana.setOnSeekBarChangeListener(seekBarManaOnSeek);

        return view;
    }

    View.OnClickListener buttonApplySkillsOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.Character.Power = seekBarPower.getProgress();
            DataTransferClass.Character.Agility = seekBarAgility.getProgress();
            DataTransferClass.Character.Mana = seekBarMana.getProgress();

            DataTransferClass.TextViewCharacterInfo.setText(DataTransferClass.Character.getStringInfo());
        }
    };

    SeekBar.OnSeekBarChangeListener seekBarPowerOnSeek = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            int power = seekBarPower.getProgress();

            textViewPower.setText("Power: "+power);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    SeekBar.OnSeekBarChangeListener seekBarAgilityOnSeek = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            int agility = seekBarAgility.getProgress();

            textViewAgility.setText("Agility: "+agility);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    SeekBar.OnSeekBarChangeListener seekBarManaOnSeek = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            int mana = seekBarMana.getProgress();

            textViewMana.setText("Mana: "+mana);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
}
